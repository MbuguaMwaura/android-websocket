package me.hhac.android.greetings.models

import java.io.Serializable

data class CommentDto(
    val commentText: String? = null,
    val commentTargetId: Long? = null,
    val imageData: String? = null,
    val commentTargetType: String? = null
) : Serializable