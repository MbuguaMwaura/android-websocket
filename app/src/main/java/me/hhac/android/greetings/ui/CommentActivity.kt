package me.hhac.android.greetings.ui

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_greeting.*
import me.hhac.android.greetings.ListRecyclerAdapter
import me.hhac.android.greetings.R
import me.hhac.android.greetings.models.CommentDto
import me.hhac.android.greetings.models.CommentResponseDto
import org.json.JSONObject


class CommentActivity : AppCompatActivity() {

    private var mSocket: Socket? = null

    private var token = "f0556144-9bcf-4bab-ba3d-56f588495dc1"

    var listView: RecyclerView? = null

    var messages : ArrayList<CommentResponseDto> = ArrayList()

    var recyclerAdapter : ListRecyclerAdapter? = null

    val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_greeting)
        listView = findViewById(R.id.listview)

        recyclerAdapter = ListRecyclerAdapter(messages,this)
        listView?.adapter = recyclerAdapter
        val opts = IO.Options.builder().setPath("/wit-forum").setTransports(arrayOf("websocket")).build()
        mSocket = try{
            IO.socket("https://c087-105-162-30-147.in.ngrok.io"

            )
        }catch (e :java.lang.Exception){
            println(e)
            null
        }

    }

    override fun onDestroy() {
        mSocket?.disconnect()
        super.onDestroy()
    }


    override fun onPause() {
        mSocket?.disconnect()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        mSocket?.connect()

        val data = JSONObject().put("discussionId",1).put("token",token)

        mSocket?.emit("joinDiscussion", data)

        mSocket?.on("comments",onNewMessage)
    }



    fun onClickSend(v: View) {

        val imm: InputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)


        //todo :  the forumTopicId is hard-coded. It should get retrieved when a user selects the discussion they want to engage in
        val commentDto = CommentDto(hello_message.text.toString(),1, commentTargetType = "COMMENT")


        val jsonObject = JSONObject()
            .put("commentText",commentDto.commentText)
            .put("commentTargetId",commentDto.commentTargetId)
            .put("commentTargetType",commentDto.commentTargetType)
            .put("imageData",commentDto.imageData)


        mSocket?.emit("chat", jsonObject);
    }



    private val onNewMessage: Emitter.Listener = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            val data = args[0] as JSONObject
            val commentReponse = gson.fromJson(data.toString(),CommentResponseDto::class.java)
            messages.add(0,commentReponse!!)
            recyclerAdapter?.notifyDataSetChanged()

        })
    }
}


