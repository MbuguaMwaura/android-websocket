package me.hhac.android.greetings.ui

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import me.hhac.android.greetings.R
import me.hhac.android.greetings.services.ForumService
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import java.io.IOException


class DiscussionActivity : AppCompatActivity() {

    private val forumService: ForumService = ForumService(OkHttpClient())


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discussion)


        val button = findViewById<Button>(R.id.button)
        button.setOnClickListener {
            forumService.getForumTopics()
        }

    }


}