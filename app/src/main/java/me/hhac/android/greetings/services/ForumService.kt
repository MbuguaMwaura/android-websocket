package me.hhac.android.greetings.services

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import me.hhac.android.greetings.models.BaseApiResponse
import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import java.io.IOException

class ForumService(private val httpClient: OkHttpClient) {
    private val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    val token = "d811771d-904a-4f68-865e-abbd6720be2f"

    @Throws(IOException::class)
    fun getForumTopics() : BaseApiResponse? {
        var responseData : BaseApiResponse? = null
        val urlBuilder: HttpUrl.Builder? = ("http://10.0.2.2:8080" + "/api/forum/topics").toHttpUrlOrNull()?.newBuilder()
        urlBuilder?.addQueryParameter("size", "3")
            ?.addQueryParameter("page","1")

        val url: String = urlBuilder?.build().toString()
        println("URL >>>>>>>>>>>>>>>  ${url}")
        val request = Request.Builder()
            .url(url)
            .addHeader("Authorization","Bearer $token")
            .build()
        val responseCallBack = object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                responseData = null
                println("FAILED ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                responseData = moshi.adapter(BaseApiResponse::class.java).fromJson(response.body.toString())
                println("SUCCESS ${response.body}")
            }

        }
        val call = httpClient.newCall(request)
        call.enqueue(responseCallBack)
        return responseData
    }
}